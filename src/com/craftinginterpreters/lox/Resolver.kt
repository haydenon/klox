package com.craftinginterpreters.lox

import java.util.*

class Resolver(private val interpreter: Interpreter) : Expr.Visitor<Unit>, Stmt.Visitor<Unit> {
    private val scopes = Stack<MutableMap<String, Boolean>>()
    private var currentFunction = FunctionType.NONE
    private var currentClass = ClassType.NONE

    override fun visitBinaryExpr(expr: Expr.Binary) {
        resolve(expr.left)
        resolve(expr.right)
    }

    override fun visitLiteralExpr(expr: Expr.Literal) = Unit

    override fun visitLogicalExpr(expr: Expr.Logical) {
        resolve(expr.left)
        resolve(expr.right)
    }

    override fun visitUnaryExpr(expr: Expr.Unary) = resolve(expr.right)

    override fun visitGroupingExpr(expr: Expr.Grouping) = resolve(expr.expr)

    override fun visitTernaryExpr(expr: Expr.Ternary) {
        resolve(expr.condition)
        resolve(expr.ifTrue)
        resolve(expr.ifFalse)
    }

    override fun visitVariableExpr(expr: Expr.Variable) {
        if (!scopes.isEmpty() &&
                scopes.peek()[expr.name.lexeme] == false) {
            throw ResolveError(expr.name, "Cannot read local variable in its own initializer.")
        }

        resolveLocal(expr, expr.name)
    }

    override fun visitAssignExpr(expr: Expr.Assign) {
        resolve(expr.value)
        resolveLocal(expr, expr.name)
    }

    override fun visitCallExpr(expr: Expr.Call) {
        resolve(expr.callee)
        expr.arguments.forEach { resolve(it) }
    }

    override fun visitAnonExpr(expr: Expr.AnonymousFunction) {
        resolveFunction(expr.parameters, expr.body, FunctionType.FUNCTION)
    }

    override fun visitSuperExpr(expr: Expr.Super) {
        if (currentClass == ClassType.NONE) {
            throw ResolveError(expr.keyword, "Cannot use 'super' outside of a class.")
        } else if (currentClass != ClassType.NONE) {
            throw ResolveError(expr.keyword, "Cannot use 'super' in a class with no superclass.")
        }

        resolveLocal(expr, expr.keyword)
    }

    override fun visitExpressionStmt(stmt: Stmt.Expression) = resolve(stmt.expression)

    override fun visitPrintStmt(stmt: Stmt.Print) = resolve(stmt.expression)

    override fun visitVarStmt(stmt: Stmt.Var) {
        declare(stmt.name)
        if (stmt.initializer != null) {
            resolve(stmt.initializer)
        }
        define(stmt.name)
    }

    override fun visitBlockStmt(stmt: Stmt.Block) {
        beginScope()
        stmt.statements.forEach { resolve(it) }
        endScope()
    }

    override fun visitIfStmt(stmt: Stmt.If) {
        resolve(stmt.condition)
        resolve(stmt.thenBranch)
        if (stmt.elseBranch != null) {
            resolve(stmt.elseBranch)
        }
    }

    override fun visitWhileStmt(stmt: Stmt.While) {
        resolve(stmt.condition)
        resolve(stmt.body)
    }

    override fun visitForStmt(stmt: Stmt.For) {
        if (stmt.init != null) resolve(stmt.init)
        if (stmt.inc != null) resolve(stmt.inc)
        resolve(stmt.condition)
        resolve(stmt.body)
    }

    override fun visitFunctionStmt(stmt: Stmt.Function) {
        declare(stmt.name)
        define(stmt.name)

        resolveFunction(stmt.parameters, stmt.body, FunctionType.FUNCTION)
    }

    override fun visitClassStmt(stmt: Stmt.Class) {
        declare(stmt.name)
        define(stmt.name)

        val enclosing = currentClass
        currentClass = ClassType.CLASS

        if (stmt.superclass != null) {
            currentClass = ClassType.SUBCLASS
            resolve(stmt.superclass)
            beginScope()
            scopes.peek().put("super", true)
        }

        for (staticMethod in stmt.staticMethods) {
            resolveFunction(staticMethod.parameters, staticMethod.body, FunctionType.STATIC)
        }

        beginScope()
        scopes.peek().put("this", true)

        for (method in stmt.methods) {
            val declaration = if (method.name.lexeme == "init") FunctionType.INITIALIZER
                              else FunctionType.METHOD
            resolveFunction(method.parameters, method.body, declaration)
        }

        endScope()

        if (stmt.superclass != null) endScope()

        currentClass = enclosing
    }

    override fun visitThisExpr(expr: Expr.This) {
        if (currentClass == ClassType.NONE || currentFunction == FunctionType.STATIC) {
            throw ResolveError(expr.keyword,
                    "Must use this in a class's instance methods")
        }
        resolveLocal(expr, expr.keyword)
    }

    override fun visitGetExpr(expr: Expr.Get) {
        resolve(expr.obj)
    }

    override fun visitSetExpr(expr: Expr.Set) {
        resolve(expr.value)
        resolve(expr.obj)
    }

    override fun visitReturnStmt(stmt: Stmt.Return){
        if (currentFunction == FunctionType.NONE) {
            throw ResolveError(stmt.keyword, "Cannot return from top-level code.")
        }

        if (stmt.value != null) {
            if (currentFunction == FunctionType.INITIALIZER)
                throw ResolveError(stmt.keyword, "Cannot return a value from an initializer.")

            resolve(stmt.value)
        }
    }

    override fun visitBreakStmt(stmt: Stmt.Break) = Unit

    override fun visitContinueStmt(stmt: Stmt.Continue) = Unit

    fun resolve(stmt: Stmt) : ResolveError? {
        try {
            stmt.accept(this)
        } catch (error: ResolveError) {
            return error
        }

        return null
    }

    private fun resolve(expr: Expr) = expr.accept(this)

    private fun resolveLocal(expr: Expr, name: Token) {
        for (i in scopes.size - 1 downTo 0) {
            if (scopes[i].containsKey(name.lexeme)) {
                interpreter.resolve(expr, scopes.size - 1 - i)
                return
            }
        }

        // Not found. Assume it is global.
    }

    private fun resolveFunction(parameters: List<Token>, body: List<Stmt>, type: FunctionType) {
        val enclosingFunction = currentFunction
        currentFunction = type

        beginScope()
        for (param in parameters) {
            declare(param)
            define(param)
        }
        body.forEach { resolve(it) }
        endScope()

        currentFunction = enclosingFunction
    }


    private fun beginScope() = scopes.push(mutableMapOf())

    private fun endScope() = scopes.pop()

    private fun declare(name: Token) {
        if (scopes.isEmpty()) {
            return
        }

        val scope = scopes.peek()

        if (scope.containsKey(name.lexeme)) {
           throw ResolveError(name,
                    "Variable with this name already declared in this scope.");
        }


        scope[name.lexeme] = false
    }

    private fun define(name: Token) {
        if (scopes.isEmpty()) {
           return
        }

        scopes.peek()[name.lexeme] = true
    }

    data class ResolveError(val token: Token, override val message: String) : Exception(message)

    private enum class FunctionType {
        NONE,
        FUNCTION,
        INITIALIZER,
        STATIC,
        METHOD
    }

    private enum class ClassType {
        NONE,
        CLASS,
        SUBCLASS
    }
}
package com.craftinginterpreters.lox

class ContinueException : Exception()

class BreakException : Exception()

package com.craftinginterpreters.lox

enum class FunctionType {
    ANONYMOUS,
    INITIALIZER,
    METHOD,
    FUNCTION,
    STATIC,
    GETTER
}

class LoxFunction(private val parameters: List<Token>,
                  private val body: List<Stmt>,
                  private val name: String?,
                  private val closure: Environment,
                  val type: FunctionType = FunctionType.FUNCTION): LoxCallable {
    constructor(declaration: Stmt.Function, closure: Environment, type: FunctionType = FunctionType.FUNCTION)
            : this(declaration.parameters, declaration.body, declaration.name.lexeme, closure, type)

    constructor(declaration: Expr.AnonymousFunction, closure: Environment)
            : this(declaration.parameters, declaration.body, null, closure)

    override fun call(interpreter: Interpreter, arguments: List<Any>): Any {
        val env = Environment(closure)
        for ((index, parameter) in parameters.withIndex()){
            env.define(parameter.lexeme, arguments[index])
        }

        try {
            interpreter.executeBlock(body, env)
        } catch (returnValue: Return) {
            return returnValue.value
        }

        if (type == FunctionType.INITIALIZER) return closure.getAt(0, "this")

        return Nil()
    }

    fun bind(instance: LoxInstance) : LoxFunction {
        val env = Environment(closure)
        env.define("this", instance)
        return LoxFunction(parameters, body, name, env, type)
    }

    override fun arity(): Int = parameters.size

    override fun toString(): String = "<fn ${ if (name != null) name else "[ANON]"}>"
}
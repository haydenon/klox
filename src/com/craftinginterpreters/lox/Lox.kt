package com.craftinginterpreters.lox

import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths

fun main(args: Array<String>) {
    val lox = Lox()
    if (args.size > 1) {
        println("Usage: jlox [script]")
    } else if (args.size == 1){
        lox.runFile(args[0])
    } else {
        lox.runPrompt()
    }
}

class Lox {
    private val interpreter = Interpreter()

    fun runFile(path: String) {
        val bytes = Files.readAllBytes(Paths.get(path))
        val result = run(String(bytes, Charset.defaultCharset()))
        if (result == Result.PARSE_ERROR) System.exit(65)
        if (result == Result.RUNTIME_ERROR) System.exit(70)
    }

    fun runPrompt() {
        val input = InputStreamReader(System.`in`)
        val reader = BufferedReader(input)

        while(true){
            print("> ")
            run(reader.readLine(), true)
        }
    }

    private fun run (source: String, tryExpr: Boolean = false): Result {
        val scanner = Scanner(source)
        val (tokens, errors) = scanner.scanTokens()

        for ((line, message) in errors) {
            error(line, message)
        }

        val parser = Parser(tokens)
        val (statements, parseErrors) = parser.parse()
        val (expr, exprParseErrors) =
            if (parseErrors.any() && tryExpr) parser.parseExpr()
            else Pair(null, listOf())

        val resolver = Resolver(interpreter)
        val resolveErrors : List<Resolver.ResolveError> = statements.mapNotNull { resolver.resolve(it) }

        if (parseErrors.any() && expr == null) {
            for ((token, message) in parseErrors) {
                error(token, message)
            }
        } else if (exprParseErrors.any()) {
            for ((token, message) in exprParseErrors) {
                error(token, message)
            }
        } else if (resolveErrors.any()) {
            for ((token, message) in resolveErrors) {
                error(token, message)
            }
        }

        if (errors.any() || (parseErrors.any() && expr == null) || resolveErrors.any())
            return Result.PARSE_ERROR

        val error =
            if (expr != null) interpreter.interpretExpr(expr)
            else interpreter.interpret(statements)

        if (error != null) runtimeError(error)

        return if (error != null) Result.RUNTIME_ERROR
            else Result.SUCCESS
    }

    fun runtimeError(error: RuntimeError) {
        System.err.println(error.message +
                "\n[line " + error.token.line + "]")
    }

    fun error(token: Token, message: String) {
        if (token.type === TokenType.EOF) {
            report(token.line, " at end", message)
        } else {
            report(token.line, " at '" + token.lexeme + "'", message)
        }
    }

    fun error(line: Int, message: String) {
        report(line, "", message)
    }

    private fun report(line: Int, where: String, message: String){
        System.err.println("[line $line] Error$where: $message")
    }

    private enum class Result {
        SUCCESS,
        PARSE_ERROR,
        RUNTIME_ERROR
    }
}
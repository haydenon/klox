package com.craftinginterpreters.lox

abstract class Expr {
    abstract fun <R> accept(visitor: Visitor<R>): R

    interface Visitor<out R> {
        fun visitBinaryExpr(expr: Binary): R
        fun visitLiteralExpr(expr: Literal): R
        fun visitLogicalExpr(expr: Logical): R
        fun visitUnaryExpr(expr: Unary): R
        fun visitGroupingExpr(expr: Grouping): R
        fun visitTernaryExpr(expr: Ternary): R
        fun visitVariableExpr(expr: Variable): R
        fun visitAssignExpr(expr: Assign): R
        fun visitCallExpr(expr: Call): R
        fun visitAnonExpr(expr: AnonymousFunction): R
        fun visitGetExpr(expr: Get): R
        fun visitSetExpr(expr: Set): R
        fun visitThisExpr(expr: This): R
        fun visitSuperExpr(expr: Super): R
    }

    data class Binary(val left: Expr, val op: Token, val right: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitBinaryExpr(this)
    }

    data class Unary(val op: Token, val right: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitUnaryExpr(this)
    }

    data class Literal(val value: Any) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitLiteralExpr(this)
    }

    data class Logical(val left: Expr, val operator: Token, val right: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitLogicalExpr(this)
    }

    data class Grouping(val expr: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitGroupingExpr(this)
    }

    data class Ternary(val condition: Expr, val ifTrue: Expr, val ifFalse: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitTernaryExpr(this)
    }

    data class Variable(val name: Token) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitVariableExpr(this)
    }

    data class Assign(val name: Token, val value: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitAssignExpr(this)
    }

    data class Call(val callee: Expr, val paren: Token, val arguments: List<Expr>) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitCallExpr(this)
    }

    data class AnonymousFunction(val parameters: List<Token>, val body: List<Stmt>) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitAnonExpr(this)
    }

    data class Get(val obj: Expr, val name: Token) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitGetExpr(this)
    }

    data class Set(val obj: Expr, val name: Token, val value: Expr) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitSetExpr(this)
    }

    data class This(val keyword: Token) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitThisExpr(this)
    }

    data class Super(val keyword: Token, val method: Token) : Expr() {
        override fun <R> accept(visitor: Visitor<R>): R = visitor.visitSuperExpr(this)
    }
}

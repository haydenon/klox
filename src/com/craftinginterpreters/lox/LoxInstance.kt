package com.craftinginterpreters.lox

open class LoxInstance(private val classInfo: LoxClassInfo) {
    private val properties = mutableMapOf<String, Any>()

    override fun toString() : String = classInfo.name + " instance"

    fun get(name: Token) : Any =
        when {
            properties.containsKey(name.lexeme) -> properties.getValue(name.lexeme)
            classInfo.hasMethod(name.lexeme) -> classInfo.findMethod(this, name.lexeme)
            else -> throw RuntimeError(name, "Undefined property ${name.lexeme}")
        }

    fun set(name: Token, value: Any) = properties.set(name.lexeme, value)
}

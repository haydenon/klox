package com.craftinginterpreters.lox

class Nil {
    override fun equals(other: Any?): Boolean = other is Nil

    override fun hashCode(): Int = 0
}
package com.craftinginterpreters.lox

import com.craftinginterpreters.lox.TokenType.*

class Scanner constructor(source: String) {
    val context = Context(source, mutableListOf<Token>(), mutableListOf<Error>(), 0, 0, 1)

    val keywords = hashMapOf(
        "and" to AND,
        "class" to CLASS,
        "else" to ELSE,
        "false" to FALSE,
        "for" to FOR,
        "fun" to FUN,
        "if" to IF,
        "nil" to NIL,
        "or" to OR,
        "print" to PRINT,
        "return" to RETURN,
        "continue" to CONTINUE,
        "break" to BREAK,
        "super" to SUPER,
        "this" to THIS,
        "true" to TRUE,
        "var" to VAR,
        "while" to WHILE
    )

    fun scanTokens(): Pair<List<Token>, List<Error>> {
        while(!isAtEnd()){
            context.start = context.current
            scanToken()
        }

        context.tokens.add(Token(EOF, "", Nil(), context.line))
        return Pair(context.tokens, context.errors)
    }

    private fun scanToken() {
        val c = advance()
        when (c) {
            '(' -> addToken(LEFT_PAREN)
            ')' -> addToken(RIGHT_PAREN)
            '{' -> addToken(LEFT_BRACE)
            '}' -> addToken(RIGHT_BRACE)
            ',' -> addToken(COMMA)
            '.' -> addToken(DOT)
            '-' -> addToken(MINUS)
            '+' -> addToken(PLUS)
            ';' -> addToken(SEMICOLON)
            '*' -> addToken(STAR)
            '?' -> addToken(QUESTION_MARK)
            ':' -> addToken(COLON)
            '!' -> addToken(if (match('=')) BANG_EQUAL else BANG)
            '=' -> addToken(if (match('=')) EQUAL_EQUAL else EQUAL)
            '<' -> addToken(if (match('=')) LESS_EQUAL else LESS)
            '>' -> addToken(if (match('=')) GREATER_EQUAL else GREATER)
            '/' -> {
                if (match('/')) {
                    while (peek() != '\n' && !isAtEnd()) advance()
                } else if (match('*')) multiLineComment()
                else addToken(SLASH)
            }
            ' ' -> {}
            '\r' -> {}
            '\t' -> {}
            '\n' -> context.line++
            '"' -> string()
            in '0'..'9' -> number()
            in 'a'..'z', in 'A'..'Z', '_' -> identifier()
            else -> context.errors.add(Error(context.line, "Unexpected character: $c"))
        }
    }

    private fun multiLineComment(){
        var level = 1
        while (level != 0 && !isAtEnd()){
            if (match('/') && match('*')) level++
            else {
                if (match('*') && match('/')) level--
                else advance()
            }
        }
    }

    private fun string() {
        while(peek() != '"' && !isAtEnd()){
            if (peek() == '"') context.line++
            advance()
        }

        if (isAtEnd()) {
            context.errors.add(Error(context.line, "Unterminated string."))
            return
        }

        advance()

        val value = context.source.substring(context.start + 1, context.current -1)
        addToken(STRING, value)
    }

    private fun number() {
        while (isDigit(peek())) advance()

        // Look for a fractional part.
        if (peek() == '.' && isDigit(peekNext())) {
            // Consume the "."
            advance()

            while (isDigit(peek())) advance()
        }

        addToken(NUMBER, context.source.substring(context.start, context.current).toDouble())
    }

    private fun identifier() {
        while (isAlphaNumeric(peek())) advance()

        val text = context.source.substring(context.start, context.current)
        val type = keywords.getOrDefault(text, IDENTIFIER)
        addToken(type)
    }

    private fun isAlphaNumeric(c: Char): Boolean {
        return isAlpha(c) || isDigit(c)
    }

    private fun isDigit(c: Char): Boolean {
        return c in '0'..'9'
    }

    private fun isAlpha(c: Char): Boolean {
        return c in 'a'..'z' || c in 'A'..'Z' || c == '_'
    }

    private fun isAtEnd(): Boolean {
        return context.current >= context.source.length
    }

    private fun match(expected: Char): Boolean{
        if (isAtEnd()) return false
        if (peek() != expected) return false

        context.current++
        return true
    }

    private fun peek(): Char {
        if (isAtEnd()) return '\u0000'
        return context.source[context.current]
    }

    private fun peekNext(): Char {
        if (context.current + 1 >= context.source.length) return '\u0000'
        return context.source[context.current + 1]
    }

    private fun advance(): Char {
        context.current++
        return context.source[context.current - 1]
    }

    private fun addToken(type: TokenType) {
        addToken(type, Nil())
    }

    private fun addToken(type: TokenType, literal: Any) {
        val text = context.source.substring(context.start, context.current)
        context.tokens.add(Token(type, text, literal, context.line))
    }

    data class Context(
        val source: String,
        val tokens: MutableList<Token>,
        val errors: MutableList<Error>,
        var start: Int,
        var current: Int,
        var line: Int)
}


package com.craftinginterpreters.lox

class AstPrinter : Expr.Visitor<String> {
    override fun visitAssignExpr(expr: Expr.Assign): String
        = parenthesize("assign ${expr.name.lexeme}", expr.value)

    override fun visitVariableExpr(expr: Expr.Variable): String
        = parenthesize("var ${expr.name.lexeme}")

    override fun visitBinaryExpr(expr: Expr.Binary): String
        = parenthesize(expr.op.lexeme, expr.left, expr.right)

    override fun visitLiteralExpr(expr: Expr.Literal): String
        = expr.value.toString()

    override fun visitUnaryExpr(expr: Expr.Unary): String
        = parenthesize(expr.op.lexeme, expr.right)

    override fun visitGroupingExpr(expr: Expr.Grouping): String
        = parenthesize("group", expr.expr)

    override fun visitTernaryExpr(expr: Expr.Ternary): String
        = parenthesize("ternary", expr.condition, expr.ifTrue, expr.ifFalse)

    override fun visitLogicalExpr(expr: Expr.Logical): String
        = parenthesize(expr.operator.lexeme, expr.left, expr.right)

    override fun visitCallExpr(expr: Expr.Call): String
        = parenthesize("call", expr.callee, *expr.arguments.toTypedArray())

    override fun visitAnonExpr(expr: Expr.AnonymousFunction): String
        = parenthesize("anon")

    override fun visitGetExpr(expr: Expr.Get): String
        = parenthesize("get ${expr.name}")

    override fun visitSetExpr(expr: Expr.Set): String
        = parenthesize("set ${expr.name}", expr.value)

    override fun visitThisExpr(expr: Expr.This): String
        = parenthesize("this")

    override fun visitSuperExpr(expr: Expr.Super): String
        = parenthesize("super ${expr.method.lexeme}")

    fun print(expr: Expr): String {
        return expr.accept(this)
    }

    private fun parenthesize(name: String, vararg expressions: Expr): String {
        val builder = StringBuilder()

        builder.append("(").append(name)
        for (expr in expressions) {
            builder.append(" ")
            builder.append(expr.accept(this))
        }
        builder.append(")")

        return builder.toString()
    }
}
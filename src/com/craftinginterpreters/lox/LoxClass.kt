package com.craftinginterpreters.lox

class LoxClassInfo(val name: String, private val superclass: LoxClass?, val methods: Map<String, LoxFunction>, val staticMethods: Map<String, LoxFunction> = emptyMap()){
    fun hasMethod(name: String)
        = methods.containsKey(name) || (superclass != null && superclass.hasMethod(name))

    fun findMethod(instance: LoxInstance, name: String) =
        when {
            methods.containsKey(name) -> methods.getValue(name).bind(instance)
            superclass != null -> superclass.findMethod(instance, name)
            else -> throw NoSuchElementException()
        }
}

class LoxClass(private val classInfo: LoxClassInfo)
    : LoxCallable, LoxInstance(LoxClassInfo("static class ${classInfo.name}", null, classInfo.staticMethods)) {

    override fun call(interpreter: Interpreter, arguments: List<Any>): Any {
        val instance = LoxInstance(classInfo)
        if (classInfo.hasMethod("init")){
            classInfo.findMethod(instance, "init").bind(instance).call(interpreter, arguments)
        }

        return instance
    }

    fun hasMethod(name: String): Boolean = classInfo.hasMethod(name)

    fun findMethod(instance: LoxInstance, name: String): LoxFunction = classInfo.findMethod(instance, name)

    override fun arity(): Int = classInfo.methods["init"]?.arity() ?: 0

    override fun toString(): String = classInfo.name
}
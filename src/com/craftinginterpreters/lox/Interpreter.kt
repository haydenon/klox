package com.craftinginterpreters.lox

import com.craftinginterpreters.lox.TokenType.*

class Interpreter : Expr.Visitor<Any>, Stmt.Visitor<Unit> {
    val globals = Environment()
    private var environment = globals
    private val locals = mutableMapOf<Expr, Int>()

    init {
        globals.define("clock", object : LoxCallable {
            override fun arity(): Int = 0

            override fun call(interpreter: Interpreter, arguments: List<Any>): Any
                    = System.currentTimeMillis().toDouble() / 1000.0
        })

    }

    private val names = mapOf(
            Double::class to "number",
            String::class to "string"
    )

    fun interpret(statements: List<Stmt>): RuntimeError? {
        try {
            for (stmt in statements) {
                execute(stmt)
            }
        } catch (error: RuntimeError) {
            return error
        }

        return null
    }

    fun interpretExpr(expr: Expr): RuntimeError? {
        try {
            println(stringify(evaluate(expr)))
        } catch (error: RuntimeError) {
            return error
        }

        return null
    }

    private fun stringify(obj: Any?): String {
        if (obj is Nil) return "nil"

        // Hack. Work around Java adding ".0" to integer-valued doubles.
        if (obj is Double) {
            var text = obj.toString()
            if (text.endsWith(".0")) {
                text = text.substring(0, text.length - 2)
            }
            return text
        }

        return obj.toString()
    }

    fun void(func: () -> Any): Unit {
        func()
    }

    override fun visitExpressionStmt(stmt: Stmt.Expression): Unit = void { evaluate(stmt.expression) }

    override fun visitPrintStmt(stmt: Stmt.Print): Unit = void { println(stringify(evaluate(stmt.expression))) }

    override fun visitVarStmt(stmt: Stmt.Var): Unit {
        val value = if (stmt.initializer != null) evaluate(stmt.initializer)
        else null

        environment.define(stmt.name.lexeme, value)
    }

    override fun visitFunctionStmt(stmt: Stmt.Function)
            = environment.define(stmt.name.lexeme, LoxFunction(stmt, environment))

    override fun visitBlockStmt(stmt: Stmt.Block) = executeBlock(stmt.statements, Environment(environment))

    override fun visitIfStmt(stmt: Stmt.If) {
        if (isTruthy(evaluate(stmt.condition)))
            execute(stmt.thenBranch)
        else if (stmt.elseBranch != null)
            execute(stmt.elseBranch)
    }

    override fun visitWhileStmt(stmt: Stmt.While) {
        var broken = false
        while (!broken && isTruthy(evaluate(stmt.condition))) {
            try {
                execute(stmt.body)
            } catch (e: BreakException) {
                broken = true
            } catch (e: ContinueException) {
            }
        }
    }

    override fun visitForStmt(stmt: Stmt.For) {
        if (stmt.init != null) execute(stmt.init)

        var broken = false
        while (!broken && isTruthy(evaluate(stmt.condition))) {
            try {
                execute(stmt.body)
                if (stmt.inc != null) execute(stmt.inc)
            } catch (e: BreakException) {
                broken = true
            } catch (e: ContinueException) {
                if (stmt.inc != null) execute(stmt.inc)
            }
        }
    }

    override fun visitReturnStmt(stmt: Stmt.Return)
            = throw Return(if (stmt.value != null) evaluate(stmt.value) else Nil())

    override fun visitBreakStmt(stmt: Stmt.Break) {
        throw BreakException()
    }

    override fun visitContinueStmt(stmt: Stmt.Continue) {
        throw ContinueException()
    }

    override fun visitBinaryExpr(expr: Expr.Binary): Any {
        val left = evaluate(expr.left)
        val right = evaluate(expr.right)

        when (expr.op.type) {
            MINUS -> return checkNumbers(expr.op, left, right) { l, r -> l - r }
            PLUS -> {
                if (left is Double && right is Double) return left + right
                else if (left is String || right is String) return stringify(left) + stringify(right)
            }
            SLASH -> {
                return checkNumbers(expr.op, left, right) { l, r ->
                    if (r == 0.0) throw RuntimeError(expr.op, "Divide by zero error")
                    else l / r
                }
            }
            STAR -> return checkNumbers(expr.op, left, right) { l, r -> l * r }
            GREATER -> return compare(expr.op, left, right)
            GREATER_EQUAL -> return compare(expr.op, left, right)
            LESS -> return compare(expr.op, left, right)
            LESS_EQUAL -> return compare(expr.op, left, right)
            BANG_EQUAL -> return !isEqual(left, right)
            EQUAL_EQUAL -> return isEqual(left, right)
            else -> {
            }
        }

        // Unreachable.
        return Nil()
    }

    override fun visitLiteralExpr(expr: Expr.Literal): Any = expr.value

    override fun visitLogicalExpr(expr: Expr.Logical): Any {
        val left = evaluate(expr.left)

        if (expr.operator.type == OR) {
            if (isTruthy(left)) return left
        } else {
            if (!isTruthy(left)) return left
        }

        return evaluate(expr.right)
    }

    override fun visitUnaryExpr(expr: Expr.Unary): Any {
        val right = evaluate(expr.right)

        when (expr.op.type) {
            BANG -> return !isTruthy(right)
            MINUS -> return checkNumber(expr.op, right) { -it }
            else -> {
            }
        }

        // Unreachable.
        return Nil()
    }

    override fun visitCallExpr(expr: Expr.Call): Any {
        val callee = evaluate(expr.callee)
        val arguments = expr.arguments.map { evaluate(it) }

        if (callee !is LoxCallable) {
            throw RuntimeError(expr.paren, "Can only call functions and classes.")
        }
        if (arguments.size != callee.arity()) {
            throw RuntimeError(expr.paren, "Expected " +
                    callee.arity() + " arguments but got " +
                    arguments.size + ".")
        }

        return callee.call(this, arguments)
    }

    override fun visitAnonExpr(expr: Expr.AnonymousFunction): Any = LoxFunction(expr, environment)

    override fun visitClassStmt(stmt: Stmt.Class) {
        fun methodMap(methods: List<Stmt.Function>)
            = methods.associateBy(
                { it.name.lexeme },
                { LoxFunction(it, environment, it.type)})
        environment.define(stmt.name.lexeme, null)
        var superclass: LoxClass? = null
        if (stmt.superclass != null) {
            superclass = evaluate(stmt.superclass) as? LoxClass
                ?: throw RuntimeError(stmt.name, "Superclass must be a class.")
            environment = Environment(environment)
            environment.define("super", superclass)
        }

        val methods = methodMap(stmt.methods)
        val staticMethods = methodMap(stmt.staticMethods)
        val info = LoxClassInfo(stmt.name.lexeme, superclass, methods, staticMethods)

        if(superclass != null) environment = environment.enclosing!!

        environment.assign(stmt.name, LoxClass(info))
    }

    override fun visitGetExpr(expr: Expr.Get): Any {
        val obj = evaluate(expr.obj)
        if (obj is LoxInstance) {
            val item = obj.get(expr.name)
            if (item is LoxFunction && item.type == FunctionType.GETTER) {
                return item.call(this, emptyList())
            }

            return item
        }

        throw RuntimeError(expr.name,
                "Only instances have properties.")
    }

    override fun visitSetExpr(expr: Expr.Set): Any {
        val obj =
            evaluate(expr.obj) as? LoxInstance
            ?: throw RuntimeError(expr.name, "Only instances have fields.")

        val value = evaluate(expr.value)
        obj.set(expr.name, value)
        return value
    }

    override fun visitThisExpr(expr: Expr.This): Any {
        return lookupVariable(expr.keyword, expr)
    }

    override fun visitSuperExpr(expr: Expr.Super): Any {
        val distance = locals.getValue(expr)
        val superclass = environment.getAt(distance, "super") as LoxClass
        val obj = environment.getAt(distance - 1, "this") as LoxInstance
        return if (superclass.hasMethod(expr.method.lexeme))
            superclass.findMethod(obj, expr.method.lexeme)
        else
            throw RuntimeError(expr.method, "Superclass method ${expr.method.lexeme} doesn't exist")
    }

    override fun visitGroupingExpr(expr: Expr.Grouping): Any = evaluate(expr.expr)

    override fun visitTernaryExpr(expr: Expr.Ternary): Any
            = if (isTruthy(evaluate(expr.condition))) evaluate(expr.ifTrue) else evaluate(expr.ifFalse)

    override fun visitVariableExpr(expr: Expr.Variable): Any = lookupVariable(expr.name, expr)// environment.get(expr.name)

    override fun visitAssignExpr(expr: Expr.Assign): Any {
        val value = evaluate(expr.value)

        val distance = locals[expr]
        if (distance != null) {
            environment.assignAt(distance, expr.name, value)
        } else {
            globals.assign(expr.name, value)
        }

        return value
    }

    private fun numbers(vararg operands: Any): Boolean = operands.all { it is Double }

    private fun strings(vararg operands: Any): Boolean = operands.all { it is String }

    private fun <R> checkStrings(operator: Token, operand1: Any, operand2: Any, body: (String, String) -> R): R {
        if (operand1 is String && operand2 is String) {
            return body(operand1, operand2)
        }

        throw RuntimeError(operator, "Operands must be of type ${names[String::class]}.")
    }

    private fun <R> checkNumbers(operator: Token, operand1: Any, operand2: Any, body: (Double, Double) -> R): R {
        if (operand1 is Double && operand2 is Double) {
            return body(operand1, operand2)
        }

        throw RuntimeError(operator, "Operands must be of type ${names[Double::class]}.")
    }

    private fun <R> checkNumber(operator: Token, operand: Any, body: (Double) -> R): R {
        if (operand is Double) {
            return body(operand)
        }

        throw RuntimeError(operator, "Operand must be of type ${names[Double::class]}.")
    }

    private fun compare(operator: Token, left: Any, right: Any): Any {
        if (numbers(left, right)) when (operator.type) {
            GREATER -> return checkNumbers(operator, left, right) { l, r -> l > r }
            GREATER_EQUAL -> return checkNumbers(operator, left, right) { l, r -> l >= r }
            LESS -> return checkNumbers(operator, left, right) { l, r -> l < r }
            LESS_EQUAL -> return checkNumbers(operator, left, right) { l, r -> l <= r }
            else -> {
            }
        }
        else if (strings(left, right)) when (operator.type) {
            GREATER -> return checkStrings(operator, left, right) { l, r -> l > r }
            GREATER_EQUAL -> return checkStrings(operator, left, right) { l, r -> l >= r }
            LESS -> return checkStrings(operator, left, right) { l, r -> l < r }
            LESS_EQUAL -> return checkStrings(operator, left, right) { l, r -> l <= r }
            else -> {
            }
        }

        if ((left is Double && right is String)
                || (left is String && right is Double))
            throw RuntimeError(operator, "Cannot compare types ${names[Double::class]} and ${names[String::class]} together.")

        throw RuntimeError(operator, "Can only compare types ${names[Double::class]} and ${names[String::class]}.")
    }

    private fun evaluate(expr: Expr): Any {
        return expr.accept(this)
    }

    private fun execute(stmt: Stmt) {
        stmt.accept(this)
    }

    fun executeBlock(statements: List<Stmt>, environment: Environment): Unit {
        val previous = this.environment
        try {
            this.environment = environment

            for (statement in statements) {
                execute(statement)
            }
        } finally {
            this.environment = previous
        }
    }

    private fun isTruthy(obj: Any): Boolean = when (obj) {
        is Nil -> false
        is Boolean -> obj
        else -> true
    }

    private fun isEqual(a: Any?, b: Any?): Boolean = a == b

    fun resolve(expr: Expr, depth: Int) {
        locals.put(expr, depth)
    }

    private fun lookupVariable(name: Token, expr: Expr): Any {
        val distance = locals[expr]
        return if (distance != null) environment.getAt(distance, name.lexeme) else globals.get(name)
    }
}
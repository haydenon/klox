package com.craftinginterpreters.lox

import com.craftinginterpreters.lox.TokenType.*;
import com.craftinginterpreters.lox.FunctionType.*;

class Parser(val tokens: List<Token>) {
    var current = 0
    var loopLevel = 0
    var errors = mutableListOf<ParseError>()

    fun init() {
        current = 0
        errors = mutableListOf()
    }

    fun parse(): Pair<List<Stmt>, List<ParseError>> {
        init()
        val statements = mutableListOf<Stmt>()
        while(!isAtEnd()){
            try {
                statements.add(declaration())
            } catch (error: ParseException) {
                synchronize()
            }
        }

        return Pair(statements, errors)
    }

    fun parseExpr(): Pair<Expr?, List<ParseError>> {
        init()
        return try {
            Pair(expression(), listOf())
        } catch (error: ParseException) {
            Pair(null, errors)
        }
    }


    private fun classDeclaration(): Stmt {
        val name = consume("Expect class name.", IDENTIFIER)

        val superclass = if (match(LESS)) {
            consume("Expect superclass name", IDENTIFIER)
            Expr.Variable(previous())
        } else null

        consume("Expect '{' before class body.", LEFT_BRACE)

        val methods = mutableListOf<Stmt.Function>()
        val staticMethods = mutableListOf<Stmt.Function>()
        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            if (check(IDENTIFIER) && check(LEFT_BRACE, 1)) {
                methods.add(functionDeclaration(FunctionType.GETTER))
            }
            if (match(CLASS)){
                staticMethods.add(functionDeclaration(FunctionType.STATIC))
            } else {
                methods.add(functionDeclaration(FunctionType.METHOD))
            }
        }

        consume("Expect '}' after class body.", RIGHT_BRACE)

        return Stmt.Class(name, superclass, methods, staticMethods)
    }

    private fun varDeclaration(): Stmt {
        val name = consume("Expect variable name.", IDENTIFIER)

        var initializer: Expr? = null
        if (match(EQUAL)) {
            initializer = expression()
        }

        consume("Expect ';' after variable declaration.", SEMICOLON)
        return Stmt.Var(name, initializer)
    }

    private fun functionDeclaration(kind: FunctionType): Stmt.Function {
        val name = consume("Expect ${functionTypeName(kind)} name.", IDENTIFIER)
        var (parameters, body) = function(kind)
        return Stmt.Function(name, kind, parameters, body)
    }

    private fun functionTypeName(type: FunctionType)
        = when (type) {
            ANONYMOUS -> "anonymous function"
            FUNCTION -> "function"
            METHOD -> "method"
            STATIC -> "static method"
            INITIALIZER -> "initializer"
            GETTER -> "getter"
        }


    private fun function(kind: FunctionType): Pair<List<Token>, List<Stmt>> {
        val kindName = functionTypeName(kind)

        val parameters = mutableListOf<Token>()
        if (kind != GETTER) {
            consume("Expect '(' after $kindName ${if (kind != FunctionType.ANONYMOUS) "name" else ""}.", LEFT_PAREN)
            if (!check(RIGHT_PAREN)) {
                do {
                    if (parameters.size >= 8) {
                        error(peek(), "Cannot have more than 8 parameters.")
                    }

                    parameters.add(consume("Expect parameter name.", IDENTIFIER))
                } while (match(COMMA))
            }
            consume("Expect ')' after parameters.", RIGHT_PAREN)
        }
        consume("Expect '{' before $kindName body.", LEFT_BRACE)

        return Pair(parameters, block())
    }

    private fun declaration(): Stmt {
        if (match(CLASS)) return classDeclaration()
        if (match(VAR)) return varDeclaration()
        if (check(FUN) && check(IDENTIFIER, 1)){
            consume("", FUN)
            return functionDeclaration(FunctionType.FUNCTION)
        }

        return statement()
    }

    private fun statement(): Stmt {
        if (match(IF)) return ifStatement()
        if (match(PRINT)) return printStatement()
        if (match(WHILE)) return whileStatement()
        if (match(FOR)) return forStatement()
        if (match(LEFT_BRACE)) return Stmt.Block(block())
        if (match(RETURN)) return returnStatement()
        if (check(BREAK)) return breakStatement()
        if (check(CONTINUE)) return continueStatement()

        return expressionStatement()
    }

    private fun block(): List<Stmt> {
        val statements = mutableListOf<Stmt>()

        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            statements.add(declaration())
        }

        consume("Expect '}' after block.", RIGHT_BRACE)
        return statements
    }

    private fun expressionStatement(): Stmt {
        val expr = expression()
        consume("Expect ';' after expression.", SEMICOLON)
        return Stmt.Expression(expr)
    }

    private fun ifStatement(): Stmt {
        consume("Expect '(' after 'if'.", LEFT_PAREN)
        val cond = expression()
        consume("Expect ')' after if condition.", RIGHT_PAREN)

        return Stmt.If(cond, statement(), if (match(ELSE)) statement() else null)
    }

    private fun printStatement(): Stmt {
        val value = expression()
        consume("Expect ';' after value.", SEMICOLON)
        return Stmt.Print(value)
    }

    private fun whileStatement(): Stmt {
        consume("Expect '(' after 'while'.", LEFT_PAREN)
        val cond = expression()
        consume("Expect ')' after while condition.", RIGHT_PAREN)

        loopLevel++
        val whileStmt = Stmt.While(cond, statement())
        loopLevel--

        return whileStmt
    }

    private fun forStatement(): Stmt {
        consume("Expect '(' after 'for'.", LEFT_PAREN)
        val init = when {
            match(SEMICOLON) -> null
            match(VAR) -> varDeclaration()
            else -> expressionStatement()
        }

        val condition = if (!check(SEMICOLON)) expression() else Expr.Literal(true)
        consume("Expect ';' after loop condition.", SEMICOLON)
        val increment = if (!check(RIGHT_PAREN)) expression() else null
        consume("Expect ')' after for clauses.", RIGHT_PAREN)

        loopLevel++
        val body = statement()
        loopLevel--

        return Stmt.For(init, condition, if (increment != null) Stmt.Expression(increment) else null, body)
    }

    private fun returnStatement(): Stmt {
        val keyword = previous()
        val value = if (check(SEMICOLON)) null else expression()
        consume("Expect ';' after return value.", SEMICOLON)

        return Stmt.Return(keyword, value)
    }

    private fun continueStatement(): Stmt {
        if (loopLevel < 1) throw error(peek(), "Can only use continue statement in loop")
        advance()
        consume("Expect ';' after expression.", SEMICOLON)

        return Stmt.Continue()
    }

    private fun breakStatement(): Stmt {
        if (loopLevel < 1) throw error(peek(), "Can only use break statement in loop")
        advance()
        consume("Expect ';' after expression.", SEMICOLON)

        return Stmt.Break()
    }

    private fun expression(): Expr = assignment()

    private fun assignment(): Expr {
        val expr = ternary()

        if (match(EQUAL)) {
            val equals = previous()
            val value = assignment()

            if (expr is Expr.Variable){
                return Expr.Assign(expr.name, value)
            } else if (expr is Expr.Get) {
                return Expr.Set(expr.obj, expr.name, value)
            }

            error(equals, "Invalid assignment target.")
        }

        return expr
    }

    private fun ternary(): Expr {
        var expr = or()

        if (match(QUESTION_MARK)){
            val ifTrue = ternary()
            consume("Expect ':' after true ternary condition.", COLON)
            val ifFalse = ternary()

            expr = Expr.Ternary(expr, ifTrue, ifFalse)
        }

        return expr
    }

    private fun binary(exprFactory: () -> Expr, vararg types: TokenType): Expr {
        if (match(*types)){
            val unexpected = previous()
            error(unexpected, "Expected expression on left side of binary operator")
        }

        var expr = exprFactory()

        while (match(*types)){
            val op = previous()
            val right = exprFactory()
            expr = Expr.Binary(expr, op, right)
        }

        return expr
    }

    private fun or(): Expr {
        var expr = and()

        while (match(OR)) {
            val operator = previous()
            val right = and()
            expr = Expr.Logical(expr, operator, right)
        }

        return expr

    }

    private fun and(): Expr {
        var expr = equality()

        while (match(AND)) {
            val operator = previous()
            val right = equality()
            expr = Expr.Logical(expr, operator, right)
        }

        return expr
    }

    private fun equality(): Expr = binary({ comparison() }, BANG_EQUAL, EQUAL_EQUAL)

    private fun comparison(): Expr = binary({ term() }, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)

    private fun term(): Expr = binary({ factor() }, MINUS, PLUS)

    private fun factor(): Expr = binary({ unary() }, SLASH, STAR)

    private fun unary(): Expr {
        if (match(BANG, MINUS)) {
            val op = previous()
            val right = unary()
            return Expr.Unary(op, right)
        }

        return call()
    }

    private fun call(): Expr {
        var expr = anonymousFunction()

        while (true) {
            expr = if (match(LEFT_PAREN)) {
                finishCall(expr)
            } else if (match(DOT)){
                val name = consume("Expect property name after '.'.", IDENTIFIER)
                Expr.Get(expr, name)
            } else {
                break
            }
        }

        return expr
    }

    private fun finishCall(callee: Expr): Expr {
        val args = mutableListOf<Expr>()
        if (!check(RIGHT_PAREN)) {
            do {
                if (args.size >= 8) {
                    error(peek(), "Cannot have more than 8 arguments.")
                }
                args.add(expression())
            } while(match(COMMA))
        }

        val paren = consume("Expect ')' after arguments.", RIGHT_PAREN)

        return Expr.Call(callee, paren, args)
    }

    private fun anonymousFunction(): Expr {
        if (match(FUN)) {
            val (parameters, body) = function(FunctionType.ANONYMOUS)
            return Expr.AnonymousFunction(parameters, body)
        }

        return primary()
    }

    private fun primary(): Expr {
        if (match(FALSE)) return Expr.Literal(false)
        if (match(TRUE)) return Expr.Literal(true)
        if (match(NIL)) return Expr.Literal(Nil())

        if (match(NUMBER, STRING)) {
            return Expr.Literal(previous().literal)
        }

        if (match(SUPER)) {
            val keyword = previous()
            consume("Expect '.' after 'super'.", DOT)
            val method = consume("Expect superclass method name", IDENTIFIER)
            return Expr.Super(keyword, method)
        }

        if (match(THIS)) {
            return Expr.This(previous())
        }

        if (match(IDENTIFIER)) {
            return Expr.Variable(previous())
        }

        if (match(LEFT_PAREN)) {
            val expr = expression()
            consume("Expect ')' after expression.", RIGHT_PAREN)
            return Expr.Grouping(expr)
        }

        throw error(peek(), "Expect expression.")
    }

    private fun match(vararg types: TokenType): Boolean {
        for (type in types){
            if (check(type)){
                advance()
                return true
            }
        }

        return false
    }

    private fun check(tokenType: TokenType, position: Int = 0): Boolean
        = if ((0..position).any { tokens[current + it].type == EOF }) false else tokens[current + position].type == tokenType

    private fun advance(): Token {
        if (!isAtEnd()) current++
        return previous()
    }

    private fun isAtEnd(): Boolean {
        return peek().type === EOF
    }

    private fun peek(): Token {
        return tokens[current]
    }

    private fun previous(): Token {
        return tokens[current - 1]
    }

    private fun consume(message: String, vararg types: TokenType): Token {
        types
            .filter { check(it) }
            .forEach { return advance() }

        throw error(peek(), message)
    }

    private fun error(token: Token, message: String): ParseException {
        errors.add(ParseError(token, message))
        return ParseException()
    }

    private fun synchronize() {
        advance()

        while (!isAtEnd()) {
            if (previous().type === SEMICOLON) return

            when (peek().type) {
                CLASS, FUN, VAR, FOR, IF, WHILE, PRINT, RETURN -> return
                else -> {}
            }

            advance()
        }
    }

    private class ParseException : RuntimeException()

    data class ParseError(val token: Token, val message: String)
}
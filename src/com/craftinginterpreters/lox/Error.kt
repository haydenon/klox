package com.craftinginterpreters.lox

data class Error(val line: Int, val message: String)
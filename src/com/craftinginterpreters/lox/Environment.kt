package com.craftinginterpreters.lox

class Environment(val enclosing: Environment? = null) {
    private val values = mutableMapOf<String, Any>()

    fun define(name: String, value: Any?){
        values.put(name, value ?: Undefined())
    }

    fun assign(name: Token, value: Any): Any {
        if (values.containsKey(name.lexeme)) {
            values.put(name.lexeme, value)
            return value
        } else if (enclosing != null){
            return enclosing.assign(name, value)
        }

        throw RuntimeError(name, "Undefined variable '${name.lexeme}'.")
    }

    fun assignAt(distance: Int, name: Token, value: Any) {
        var env = this
        for (i in 0 until distance) {
            env = env.enclosing ?: throw IllegalStateException()
        }

        env.values.put(name.lexeme, value)
    }


    fun get(name: Token): Any {
        val value = values.getOrElse(name.lexeme) {
            enclosing?.get(name) ?: throw RuntimeError(name, "Undefined variable '${name.lexeme}'.")
        }

        if (value is Undefined){
            throw RuntimeError(name, "Access to an uninitialized variable '${name.lexeme}'.")
        }

        return value
    }

    fun getAt(distance: Int, name: String): Any {
        var env = this
        for (i in 0 until distance) {
            env = env.enclosing ?: throw IllegalStateException()
        }

        return env.values[name] ?: throw IllegalStateException()
    }

    private class Undefined
}